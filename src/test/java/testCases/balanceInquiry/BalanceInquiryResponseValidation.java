package testCases.balanceInquiry;

import org.testng.annotations.Test;

public class BalanceInquiryResponseValidation extends BalanceInquiryBeforeTest {
    @Test(priority=1)
    void BalanceInquiryRequest() throws Exception {
        logger.info("******************Started BalanceInquiry Test******************");

        EncryptRequest();
        ResponseHeaderValidator();
    }
}

package testCases.history;

import org.testng.annotations.Test;

public class HistoryResponseValidation extends HistoryBeforeTest {
    @Test(priority=1)
    void CustomerHistoryRequest() throws Exception {
        logger.info("******************Started CustomerHistoryRequest Test******************");

        bodyJson.replace("historyType", "01");
        bodyJson.replace("txnLimit", "y");

        EncryptRequest();
        ResponseHeaderValidator();
    }

    @Test(priority=2)
    void MerchantHistoryRequest() throws Exception {
        logger.info("******************Started MerchantHistoryRequest Test******************");

        bodyJson.replace("historyType", "02");
        bodyJson.replace("txnLimit", "");

        EncryptRequest();
        ResponseHeaderValidator();
    }
}

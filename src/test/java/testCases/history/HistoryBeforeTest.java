package testCases.history;

import base.BaseClass;
import encrypter.MobileEncrypter;
import io.restassured.RestAssured;
import org.json.simple.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class HistoryBeforeTest extends BaseClass {

    JSONObject bodyJson = new JSONObject();

    @BeforeClass
    void start(){
        logger.info("******************Started History Download Request Test******************");
    }

    @BeforeMethod
    void setUp() throws Exception {
        MobileEncrypter.init();

        RestAssured.baseURI = "https://lvtsit.boc.lk/eswitch/doProcess";

        headerJson.put("txnAquirerBankCode", txnAquirerBankCode);
        headerJson.put("deviceId", deviceId);
        headerJson.put("msgFormatVersion", msgFormatVersion);
        headerJson.put("pushId", pushId);
        headerJson.put("ticket", ticket);
        headerJson.put("traceNo", traceNo = getTraceNo());
        headerJson.put("txnDateTime", txnDateTime = getTransactionDateTime());
        headerJson.put("txnType", TXN_HISTORY_TRAN);

        bodyJson.put("historyMode", 1);
        bodyJson.put("historyType", "01");
        bodyJson.put("txnLimit", "y");

        requestParameters.put("body", bodyJson);
        requestParameters.put("header", headerJson);

    }
}

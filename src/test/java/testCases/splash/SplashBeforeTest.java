package testCases.splash;

import base.BaseClass;
import encrypter.MobileEncrypter;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import org.apache.http.HttpStatus;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.util.Iterator;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class SplashBeforeTest extends BaseClass {

    JSONObject bodyJson = new JSONObject();

    @BeforeClass
    void start(){
        logger.info("******************Started Splash Request Test******************");
    }

    @BeforeMethod
    void setUp() throws Exception {
        MobileEncrypter.init();

        RestAssured.baseURI = "https://lvtsit.boc.lk/eswitch/doProcess";

        headerJson.put("txnAquirerBankCode", txnAquirerBankCode);
        headerJson.put("deviceId", deviceId);
        headerJson.put("msgFormatVersion", msgFormatVersion);
        headerJson.put("pushId", pushId);
        headerJson.put("ticket", ticket);
        headerJson.put("traceNo", traceNo = getTraceNo());
        headerJson.put("txnDateTime", txnDateTime = getTransactionDateTime());
        headerJson.put("txnType", MOBILE_APP_KEY_VERIFICATION);

        bodyJson.put("merchantType", merchantType = 2);
        bodyJson.put("key", key = "AAAAB0ylexb58eo+");
        bodyJson.put("osType", osType = "Android");
        bodyJson.put("mobVersion", mobVersion = 157);

        requestParameters.put("body", bodyJson);
        requestParameters.put("header", headerJson);

    }

    void DeviceChangeInvalidNumberValidator() throws Exception {
        Assert.assertEquals(resJson.get("responseCode"), "O1");
        logger.info("[responseCode]" + " -----> " + resJson.get("responseCode"));
        Assert.assertEquals(resJson.get("errDsc"), "Invalid Mobile Number");
        logger.info("[errDsc]" + " -----> " + resJson.get("errDsc"));
    }

    void ResponseHeaderAdditionalFieldValidator() throws Exception {
        response = given().relaxedHTTPSValidation().contentType("application/json").body(message).when().post(baseURI).then().log().all()
                .assertThat().statusCode(HttpStatus.SC_OK).extract().response();

        JsonPath jsonPath = response.jsonPath();
        message = jsonPath.get("data");
        responseJson = DecryptResponse();
        resJson= new JsonPath(String.valueOf(responseJson));

        for(Iterator iterator = headerJson.keySet().iterator(); iterator.hasNext();) {
            String key = (String) iterator.next();
            logger.info("[" + key + "]" + ":" +headerJson.get(key) + " -----> " + resJson.get("header." + key));
            if(headerJson.get(key).equals("TestField")){

            }
            else if(!(headerJson.get(key).equals(resJson.get("header." + key)))){
                Assert.assertEquals(headerJson.get(key), resJson.get("header." + key));
            }
        }
    }

    @AfterClass
    void tearDown(){
        logger.info("******************Finished Splash Request Test******************");
    }
}

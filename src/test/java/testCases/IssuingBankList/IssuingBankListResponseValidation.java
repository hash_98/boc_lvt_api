package testCases.IssuingBankList;

import org.testng.annotations.Test;

public class IssuingBankListResponseValidation extends IssuingBankListBeforeTest{

    @Test(priority=1)
    void IssuingBankListRequest() throws Exception {
        logger.info("******************Started IssuingBankList Test******************");

        EncryptRequest();
        ResponseHeaderValidator();
        ResponseBodyValidator();
    }
}

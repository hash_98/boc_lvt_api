package testCases.logout;

import org.testng.annotations.Test;

public class LogoutResponseValidation extends LogoutBeforeTest {
    @Test(priority=1)
    void LogoutRequest() throws Exception {
        logger.info("******************Started Logout Test******************");

        EncryptRequest();
        ResponseHeaderValidator();
    }
}

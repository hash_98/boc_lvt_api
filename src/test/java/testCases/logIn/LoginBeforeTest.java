package testCases.logIn;

import base.BaseClass;
import encrypter.MobileEncrypter;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import org.apache.http.HttpStatus;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;


import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class LoginBeforeTest extends BaseClass{

    JSONObject bodyJson = new JSONObject();

    @BeforeClass
    void start(){
        logger.info("******************Started Login Request Test******************");
    }

    @BeforeMethod
    void setUp() throws Exception {

        MobileEncrypter.init();

        RestAssured.baseURI = "https://lvtsit.boc.lk/eswitch/doProcess";

        headerJson.put("txnAquirerBankCode", txnAquirerBankCode);
        headerJson.put("deviceId", deviceId);
        headerJson.put("msgFormatVersion", msgFormatVersion);
        headerJson.put("pushId", pushId);
        headerJson.put("ticket", ticket);
        headerJson.put("traceNo", traceNo = getTraceNo());
        headerJson.put("txnDateTime", txnDateTime = getTransactionDateTime());
        headerJson.put("txnType", LOGIN);

        bodyJson.put("mobVersion", 157);
        bodyJson.put("mpin", "58322123c3592d3d5138d6c6cc476cbf88d23ff6b55a06ac952e4657e18d69f3f9e2c98a2ad25224a99d8d4a993274b4620cd9df9ddb20a58a2c81ac95b4b570");

        requestParameters.put("body", bodyJson);
        requestParameters.put("header", headerJson);

    }

    void LoginInvalidMPinFirstResponse() throws Exception {

        Assert.assertEquals(resJson.get("responseCode"), "P1");
        Assert.assertEquals(resJson.get("errDsc"), "You have only 4 time left");

    }

    void LoginInvalidMPinSecondResponse() throws Exception {
        headerJson.replace("traceNo", traceNo = getTraceNo());

        response = given().relaxedHTTPSValidation().contentType("application/json").body(message).when().post(baseURI).then().log().all()
                .assertThat().statusCode(HttpStatus.SC_OK).extract().response();

        JsonPath jsonPath = response.jsonPath();
        message = jsonPath.get("data");
        JSONObject responseJson= DecryptResponse();
        JsonPath resJson= new JsonPath(String.valueOf(responseJson));

        Assert.assertEquals(resJson.get("responseCode"), "P1");
        logger.info("[responseCode]" + " -----> " + resJson.get("responseCode"));
        Assert.assertEquals(resJson.get("errDsc"), "You have only 3 time left");
        logger.info("[errDsc]" + " -----> " + resJson.get("errDsc"));
    }

    @AfterClass
    void tearDown(){
        logger.info("******************Finished Login Request Test******************");
    }
}

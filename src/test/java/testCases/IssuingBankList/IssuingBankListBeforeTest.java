package testCases.IssuingBankList;

import base.BaseClass;
import encrypter.MobileEncrypter;
import io.restassured.RestAssured;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class IssuingBankListBeforeTest extends BaseClass {

    @BeforeClass
    void start(){
        logger.info("******************Started Issuing Bank List Download Request Test******************");
    }

    @BeforeMethod
    void setUp() throws Exception {

        MobileEncrypter.init();

        RestAssured.baseURI = "https://lvtsit.boc.lk/eswitch/doProcess";

        headerJson.put("txnAquirerBankCode", txnAquirerBankCode);
        headerJson.put("deviceId", deviceId);
        headerJson.put("msgFormatVersion", msgFormatVersion);
        headerJson.put("pushId", pushId);
        headerJson.put("ticket", ticket);
        headerJson.put("traceNo", traceNo = getTraceNo());
        headerJson.put("txnDateTime", txnDateTime = getTransactionDateTime());
        headerJson.put("txnType", GET_BANK_LIST_TRAN);

        requestParameters.remove("body");
        requestParameters.put("header", headerJson);
    }

    void ResponseBodyValidator() {

        Assert.assertTrue(responseJson.toString().contains("bankList"));

    }

    @AfterClass
    void tearDown(){
        logger.info("******************Finished Issuing Bank List Download Request Test******************");
    }
}

package base;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import encrypter.MobileEncrypter;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jpos.iso.ISOUtil;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class BaseClass {

    protected static RequestSpecification httpRequest;

    public Logger logger;

    public static JSONObject requestParameters = new JSONObject();
    public static JSONObject finalJsonBody = new JSONObject();
    public static String message;

    public static Response response;

    public static JSONObject headerJson = new JSONObject();

    public static JSONObject responseJson = new JSONObject();
    public static JsonPath resJson = new JsonPath(String.valueOf(responseJson));

    //header Fields
    public static String txnAquirerBankCode = "6010";
    public static String deviceId  = "60528";
    public static String msgFormatVersion = "001";
    public static String pushId = "fstNgSruTMOTZqAuhyYy04:APA91bFcBtN2uplCujvuWCwT-dw16tVsgQLaQo-D1v6-8j7WgpqIasRTwHuubPg_GDx7AC65dTevMZYr5who3d6vTmxzX6U5B8t79lrM-eF_R6qNUt_UOSyGKxXwEad9aEHIecgGDqyb";
    public static String ticket = "00011";
    public static String traceNo;

    static {
        try {
            traceNo = getProperty("traceNo");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String txnDateTime;

    //header txnType
    public static final String MOBILE_APP_KEY_VERIFICATION = "50";
    public static final String CHANGE_DEVICE = "25";
    public static final String CHANGE_DEVICE_OTP_VERIFICATION = "40";
    public static final String LOGIN = "10";
    public static final String PROMOTION_DATA_REQUEST = "68";
    public static final String GET_BANK_LIST_TRAN = "02";
    public static final String GET_INSTRUMENT_LIST_TRAN = "05";
    public static final String ACCOUNT_BALANCE_INQ = "23";
    public static final String LOGOUT = "11";
    public static final String TXN_HISTORY_TRAN = "17";
    public static final String PAYMENT_INSTRUMENT_TRAN = "03";
    public static final String PAYMENT_VERIFY_TRAN = "04";
    public static final String MERCHANT_PAYMENT_TRAN = "06";
    public static final String WALLET_REGISTRATION_TRAN = "07";
    public static final String OTP_VERIFY_TRAN = "08";
    public static final String PIN_CREATE_TRAN = "09";
    public static final String PIN_CHANGE_TRAN = "12";
    public static final String MERCHANT_REGISTRATION_TRAN = "13";
    public static final String MERCHANT_VERIFICATION_TRAN = "14";
    public static final String MERCHANT_PAYMENT_QR_TRAN = "15";
    public static final String PUSH_RESPONSE_TRAN = "16";
    public static final String GET_SECURITY_QUESTION_LIST = "19";
    public static final String RESEND_OTP_TRAN = "28";
    public static final String WALLET_DEREGISTRATION_REQUEST = "24";
    public static final String PROFILE_EDIT_TRAN = "29";
    public static final String ACKNOWLEDGEMENT_TRAN = "32";
    public static final String DETOKENIZE_OTHER_BANK = "30";
    public static final String PAYMENT_OTHER_BANK = "31";
    public static final String ACCOUNT_REMOVE = "37";
    public static final String GET_PROFILE_DET_TRAN = "33";
    public static final String GET_MERCHANT_REG = "22";
    public static final String GET_MINI_STATEMENT = "26";
    public static final String PROCESSING_TRANSACTION = "41";
    public static final String SECURITY_OTP = "42";
    public static final String PIN_RESET_AVAILABLE = "47";
    public static final String PIN_RESET_OTP_VERIFICATION = "48";
    public static final String PIN_RESET_OTP_PIN_CREATE = "49";
    public static final String UTILITY_FIELD_INQUIRY_REQUEST = "51";
    public static final String BILLER_PAYMENT_REQUEST = "52";
    public static final String UTILITY_SERVICE_CHARGE_REQUEST = "54";
    public static final String JUSTPAY_TERMS_VERIFY = "57";
    public static final String JUSTPAY_DEVICE_CHANGE_REVOKE = "59";
    public static final String SEND_SIGNED_TERMS_DEVICE_CHANGE = "60";
    public static final String UTILITY_BILLER_LIST_INQUIRY = "61";
    public static final String BILLER_FIELD_INQUIRY = "62";
    public static final String SAVE_BILLER_INQUIRY = "63";
    public static final String GET_BILLERS_INQUIRY = "64";
    public static final String EDIT_BILLERS_INQUIRY = "65";
    public static final String DELETE_BILLERS_INQUIRY = "66";
    public static final String CHANGE_BILLER_FAVORITE = "67";
    public static final String PROFILE_IMAGE_UPLOAD_REQUEST = "70";

    //body Fields
    public static int merchantType;
    public static String key;
    public static String osType;
    public static int mobVersion;

    public static String firstName;
    public static String lastName;
    public static String nic;

    @BeforeClass
    public void setup(){

        logger= Logger.getLogger("BOC_LVT");
        PropertyConfigurator.configure("Log4j.properties");
        logger.setLevel(Level.DEBUG);

    }


    public static String getAlphaNumericString(int n) {
        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder returnString = new StringBuilder(n);

        for (int i = 0; i < n; i++) {
            // generate a random number
            int index = (int)(AlphaNumericString.length() * Math.random());

            // add Character one by one in end of sb
            returnString.append(AlphaNumericString.charAt(index));
        }

        return returnString.toString();
    }

    public static String getNumericString(int n) {
        String NumericString = "0123456789";

        StringBuilder returnString = new StringBuilder(n);

        for (int i = 0; i < n; i++) {
            int index = (int)(NumericString.length() * Math.random());
            returnString.append(NumericString.charAt(index));
        }

        return returnString.toString();
    }

    public String EncryptRequest() throws Exception {
        String prettyPrint;
        byte[] request = requestParameters.toJSONString().getBytes();
        byte[] enRequest = MobileEncrypter.packet_encrypte(request, true);
        finalJsonBody.put("data", ISOUtil.hexString(enRequest));

        message = finalJsonBody.toJSONString();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        prettyPrint = gson.toJson(requestParameters);
        logger.info(">>>>> Request Parameters: " + "\n" + prettyPrint + " >>>>>");
        return message;
    }

    public  JSONObject DecryptResponse() throws Exception {
        String prettyPrint;
        byte[] decryptResponse;
        decryptResponse = MobileEncrypter.packet_decrypte(ISOUtil.hex2byte(message));
        assert decryptResponse != null;
        String responseString = new String(decryptResponse);
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(responseString);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        prettyPrint = gson.toJson(json);
        logger.info("<<<<< Response Parameters: " + "\n" + prettyPrint + " <<<<<");
        return json;
    }

    public static String getTraceNo() {
        if (null != traceNo || !traceNo.trim().equals("")) {
            if (traceNo.equals("999999")) {
                traceNo = "1";
            } else {
                int trace = Integer.parseInt(traceNo);
                trace++;
                traceNo = String.valueOf(trace);
            }
        } else {
            traceNo = "1";
        }
        return traceNo = zeroPad(6);
    }

    public static String zeroPad(int length) {
        try {
            StringBuffer sb = new StringBuffer();

            if(traceNo.length() > length){
                sb.append(traceNo.substring(0, length));
            }
            else {
                for (int i = 0; i < (length - traceNo.length()); i++) {
                    sb.append("0");
                }
            }

            return (sb + traceNo);
        } catch (Exception e) {
            return null;
        }
    }

    public static String getTransactionDateTime() {
        DateFormat df = new SimpleDateFormat("yyyyMMddkkmmss");
        Calendar c = Calendar.getInstance();
        Date day = c.getTime();
        String s2 = df.format(day);

        return s2;
    }

    //write last traceNo
    public static void writeToFile(String WriteValue) throws IOException {
        FileWriter fw=new FileWriter("properties.properties");
        fw.write("traceNo="+WriteValue);
        try {
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //get last traceNo
    public static String getProperty(String key) throws IOException, FileNotFoundException {
        FileInputStream fis = new FileInputStream("properties.properties");
        Properties prop = new Properties();
        prop.load(fis);
        return prop.getProperty(key);

    }

    protected void ResponseHeaderValidator() throws Exception {
        response = given().relaxedHTTPSValidation().contentType("application/json").body(message).when().post(baseURI).then().log().all()
                .assertThat().statusCode(HttpStatus.SC_OK).extract().response();

        JsonPath jsonPath = response.jsonPath();
        message = jsonPath.get("data");
        responseJson = DecryptResponse();
        resJson= new JsonPath(String.valueOf(responseJson));

        for(Iterator iterator = headerJson.keySet().iterator(); iterator.hasNext();) {
            String key = (String) iterator.next();
            logger.info("[" + key + "]" + ":" +headerJson.get(key) + " -----> " + resJson.get("header." + key));
            if(!(headerJson.get(key).equals(resJson.get("header." + key)))){
                Assert.assertEquals(headerJson.get(key), resJson.get("header." + key));
            }
        }
    }
}
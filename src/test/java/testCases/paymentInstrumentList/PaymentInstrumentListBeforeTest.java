package testCases.paymentInstrumentList;

import base.BaseClass;
import encrypter.MobileEncrypter;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.util.Iterator;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class PaymentInstrumentListBeforeTest extends BaseClass {

    JSONObject bodyJson = new JSONObject();

    @BeforeClass
    void start(){
        logger.info("******************Started Payment Instrument List Download Request Test******************");
    }

    @BeforeMethod
    void setUp() throws Exception {

        MobileEncrypter.init();

        RestAssured.baseURI = "https://lvtsit.boc.lk/eswitch/doProcess";

        headerJson.put("txnAquirerBankCode", txnAquirerBankCode);
        headerJson.put("deviceId", deviceId);
        headerJson.put("msgFormatVersion", msgFormatVersion);
        headerJson.put("pushId", pushId);
        headerJson.put("ticket", ticket);
        headerJson.put("traceNo", traceNo = getTraceNo());
        headerJson.put("txnDateTime", txnDateTime = getTransactionDateTime());
        headerJson.put("txnType", GET_INSTRUMENT_LIST_TRAN);

        bodyJson.put("mid", "100000000000001");

        requestParameters.put("header", headerJson);
        requestParameters.put("body", bodyJson);
    }

    @AfterClass
    void tearDown(){
        logger.info("******************Finished Payment Instrument List Download Request Test******************");
    }
}

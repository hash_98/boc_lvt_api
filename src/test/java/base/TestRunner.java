package base;

import org.testng.TestNG;
import testCases.IssuingBankList.IssuingBankListResponseValidation;
import testCases.balanceInquiry.BalanceInquiryResponseValidation;
import testCases.logIn.LoginResponseValidation;
import testCases.logout.LogoutResponseValidation;
import testCases.paymentInstrumentList.PaymentInstrumentListResponseValidation;
import testCases.promotion.PromotionDownloadResponseValidator;
import testCases.splash.SplashResponseValidations;

public class TestRunner {

    static TestNG testNG;

    public static void main(String[] args) {
        testNG = new TestNG();

        testNG.setTestClasses(new Class[] {
                SplashResponseValidations.class,
                LoginResponseValidation.class,
                PromotionDownloadResponseValidator.class,
                IssuingBankListResponseValidation.class,
                PaymentInstrumentListResponseValidation.class,
                BalanceInquiryResponseValidation.class,
                LogoutResponseValidation.class
        });
        testNG.run();
    }
}

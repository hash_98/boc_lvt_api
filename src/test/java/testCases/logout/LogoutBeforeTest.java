package testCases.logout;

import base.BaseClass;
import encrypter.MobileEncrypter;
import io.restassured.RestAssured;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;

public class LogoutBeforeTest extends BaseClass {

    @BeforeClass
    void start(){
        logger.info("******************Started Logout Request Test******************");
    }

    @BeforeMethod
    void setUp() throws Exception {

        MobileEncrypter.init();

        RestAssured.baseURI = "https://lvtsit.boc.lk/eswitch/doProcess";

        headerJson.put("txnAquirerBankCode", txnAquirerBankCode);
        headerJson.put("deviceId", deviceId);
        headerJson.put("msgFormatVersion", msgFormatVersion);
        headerJson.put("pushId", pushId);
        headerJson.put("ticket", ticket);
        headerJson.put("traceNo", traceNo = getTraceNo());
        headerJson.put("txnDateTime", txnDateTime = getTransactionDateTime());
        headerJson.put("txnType", LOGOUT);

        requestParameters.remove("body");
        requestParameters.put("header", headerJson);

    }

    @AfterClass
    void tearDown() throws IOException {
        writeToFile(traceNo);
        logger.info("******************Finished Logout Request Test******************");
    }
}

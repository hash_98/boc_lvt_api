package testCases.promotion;

import base.BaseClass;
import encrypter.MobileEncrypter;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.jpos.iso.ISOUtil;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PromotionDownloadResponseValidator extends PromotionDownloadBeforeTest {

    @Test
    void PromotionDownloadTest() throws Exception {
        logger.info("******************Started PromotionDownload Test******************");

        EncryptRequest();
        ResponseHeaderValidator();
    }
}

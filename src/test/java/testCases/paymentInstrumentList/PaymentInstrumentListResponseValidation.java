package testCases.paymentInstrumentList;

import org.testng.annotations.Test;

public class PaymentInstrumentListResponseValidation extends PaymentInstrumentListBeforeTest{
    @Test(priority=1)
    void PaymentInstrumentListRequest() throws Exception {
        logger.info("******************Started PaymentInstrumentList Test******************");

        EncryptRequest();
        ResponseHeaderValidator();
    }
}

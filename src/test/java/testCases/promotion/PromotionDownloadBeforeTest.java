package testCases.promotion;

import base.BaseClass;
import encrypter.MobileEncrypter;
import io.restassured.RestAssured;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class PromotionDownloadBeforeTest extends BaseClass {

    public static JSONObject bodyJson = new JSONObject();

    @BeforeClass
    void start(){
        logger.info("******************Started Promotion Download Request Test******************");
    }

    @BeforeMethod
    void setUp() throws Exception {

        MobileEncrypter.init();

        RestAssured.baseURI = "https://lvtsit.boc.lk/eswitch/doProcess";

        headerJson.put("txnAquirerBankCode", txnAquirerBankCode);
        headerJson.put("deviceId", deviceId);
        headerJson.put("msgFormatVersion", msgFormatVersion);
        headerJson.put("pushId", pushId);
        headerJson.put("ticket", ticket);
        headerJson.put("traceNo", traceNo = getTraceNo());
        headerJson.put("txnDateTime", txnDateTime = getTransactionDateTime());
        headerJson.put("txnType", PROMOTION_DATA_REQUEST);

        bodyJson.put("homeView", 1);

        requestParameters.put("body", bodyJson);
        requestParameters.put("header", headerJson);

    }

    @AfterClass
    void tearDown(){
        logger.info("******************Finished Promotion Download Request Test******************");
    }
}

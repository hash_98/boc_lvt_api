package testCases.logIn;

import org.testng.annotations.Test;

public class LoginResponseValidation extends LoginBeforeTest{

    @Test(priority=1)
    void InvalidMPinLogin() throws Exception {
        logger.info("******************Started InvalidMPinLogin Test******************");

        bodyJson.replace("mpin", "bbbbbbbbbbbbbbbbbbbbbbbbbbbb");

        EncryptRequest();
        ResponseHeaderValidator();
        LoginInvalidMPinFirstResponse();
//        LoginInvalidMPinSecondResponse();
    }

    @Test(priority=2)
    void LoginRequest() throws Exception {
        logger.info("******************Started Login Test******************");

        EncryptRequest();
        ResponseHeaderValidator();
    }
}

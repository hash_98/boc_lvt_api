package testCases.splash;

import org.testng.annotations.Test;


public class SplashResponseValidations extends SplashBeforeTest {


    @Test(priority=1)
    void NoUserSplashRequest() throws Exception {
        logger.info("******************Started SplashNoUserRequest Test******************");

        headerJson.replace("txnAquirerBankCode", "");
        headerJson.replace("deviceId", "0");

        EncryptRequest();
        ResponseHeaderValidator();
    }

    @Test(priority=2)
    void  DeviceChangeRequestInvalidNumber() throws Exception {
        logger.info("******************Started SplashDeviceChangeRequest Test******************");

        bodyJson.remove("merchantType");
        bodyJson.remove("key");
        bodyJson.remove("osType");
        bodyJson.remove("mobVersion");

        bodyJson.put("mobileNo", "0000000000");

        headerJson.replace("txnAquirerBankCode", "");
        headerJson.replace("deviceId", "0");
        headerJson.replace("txnType", CHANGE_DEVICE);

        EncryptRequest();
        ResponseHeaderValidator();
        DeviceChangeInvalidNumberValidator();

        bodyJson.remove("mobileNo");
    }

    @Test(priority=3)
    void  DeviceChangeRequest() throws Exception {
        logger.info("******************Started SplashDeviceChangeRequest Test******************");

        bodyJson.remove("merchantType");
        bodyJson.remove("key");
        bodyJson.remove("osType");
        bodyJson.remove("mobVersion");

        bodyJson.put("mobileNo", "0779855171");

        headerJson.replace("txnAquirerBankCode", "");
        headerJson.replace("deviceId", "0");
        headerJson.replace("txnType", CHANGE_DEVICE);

        EncryptRequest();
        ResponseHeaderValidator();

        bodyJson.remove("mobileNo");
    }
//
//    @Test(priority=4)
//    void DeviceChangeOTPRequest() throws Exception {
//        logger.info("******************Started WithUserSplashRequest Test******************");
//
//        bodyJson.remove("merchantType");
//        bodyJson.remove("key");
//        bodyJson.remove("mobVersion");
//
//        bodyJson.put("brand", "Xiaomi");
//        bodyJson.put("mobileNo", "0779855171");
//        bodyJson.put("model", "Redmi Note 6 Pro");
//        bodyJson.put("osVersion","P");
//        bodyJson.put("osType","Android");
//        bodyJson.put("otp","83189974");
//        bodyJson.put("uuid", "4427b13be8cd499993de540e34c96642");
//
//        headerJson.replace("txnType", CHANGE_DEVICE_OTP_VERIFICATION);
//
//        EncryptRequest();
//        ResponseHeaderValidator();
//
//        bodyJson.remove("brand");
//        bodyJson.remove("mobileNo");
//        bodyJson.remove("model");
//        bodyJson.remove("osVersion");
//        bodyJson.remove("osType");
//        bodyJson.remove("otp");
//        bodyJson.remove("uuid");
//
//    }

    @Test(priority=5)
    void WithUserSplashRequest() throws Exception {
        logger.info("******************Started WithUserSplashRequest Test******************");

        EncryptRequest();
        ResponseHeaderValidator();
    }

    @Test(priority=6)
    void SplashInvalidKeyRequest() throws Exception {
        logger.info("******************Started Splash InvalidKeyRequest Test******************");

        bodyJson.replace("key", "BBBBBBBBBBBBBB+");

        EncryptRequest();
        ResponseHeaderValidator();
    }

    @Test(priority=7)
    void SplashInvalidTraceNoRequest() throws Exception {
        logger.info("******************Started Splash InvalidTraceNoRequest Test******************");

        headerJson.replace("traceNo", "traceNo = getTraceNo()");

        EncryptRequest();
        ResponseHeaderValidator();
    }

    @Test(priority=8)
    void SplashInvalidTxnDateTimeRequest() throws Exception {
        logger.info("******************Started Splash InvalidTraceNoRequest Test******************");

        headerJson.replace("txnDateTime", "txnDateTime = getTransactionDateTime()");

        EncryptRequest();
        ResponseHeaderValidator();
    }

    @Test(priority=9)
    void SplashAdditionalFieldRequest() throws Exception {
        logger.info("******************Started Splash AdditionalFieldRequest Test******************");

        bodyJson.put("TestField", "TestField");
        headerJson.put("TestField", "TestField");

        EncryptRequest();
        ResponseHeaderAdditionalFieldValidator();

        bodyJson.remove("TestField", "TestField");
        headerJson.remove("TestField", "TestField");
    }

    @Test(priority=10)
    void SplashMissingDeviceIdRequest() throws Exception {
        logger.info("******************Started Splash MissingDeviceIdRequest Test******************");

        headerJson.remove("deviceId");

        EncryptRequest();
        ResponseHeaderValidator();
    }
}